from flask import Flask
import requests

app = Flask(__name__)

@app.route('/')
def index():
    response_bd = requests.get("http://database/recurso")
    return str(response_bd.json())
